﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ShootStuffTwo
{
    /// <summary>
    /// Single service object that manages updates for all registered Entities, and performs various
    /// collision detection and game rule enforcement responsibilities between Entities
    /// </summary>
    class EntityManager : IPlayerManager, IEnemyManager, IPlayerProjectileManager
    {
        /// <summary>
        /// List of registered players
        /// </summary>
        private List<PlayerEntity> playerEntities = new List<PlayerEntity>();

        /// <summary>
        /// List of registered enemies
        /// </summary>
        private List<EnemyEntity> enemyEntities = new List<EnemyEntity>();

        /// <summary>
        /// List of registered player projectiles
        /// </summary>
        private List<ProjectileEntity> playerProjectiles = new List<ProjectileEntity>();

        public EntityManager()
        {

        }

        public void Update(GameTime gameTime)
        {
            // Update all players
            foreach (PlayerEntity player in playerEntities)
            {
                player.Update(gameTime);
            }

            // Update all enemies
            foreach (EnemyEntity enemy in enemyEntities)
            {
                enemy.Update(gameTime);
            }

            // Update all projectiles
            foreach (ProjectileEntity projectile in playerProjectiles)
            {
                projectile.Update(gameTime);
            }

            // Check for collision between the player projectiles and enemies
            foreach (ProjectileEntity projectile in playerProjectiles)
            {
                foreach (EnemyEntity enemy in enemyEntities)
                {
                    if (projectile.Active &&
                        enemy.Active &&
                        projectile.CollisionRectangle.Intersects(enemy.CollisionRectangle))
                    {
                        // This projectile is hitting this enemy!
                        // TODO:  BOOM!
                        // TODO:  SCORE!

                        // Deactivate both
                        projectile.Active = false;
                        enemy.Active = false;
                    }
                }
            }


            // Check for collision between the enemies and the players
            foreach (PlayerEntity player in playerEntities)
            {
                foreach (EnemyEntity enemy in enemyEntities)
                {
                    if (player.Active &&
                        enemy.Active &&
                        player.CollisionRectangle.Intersects(enemy.CollisionRectangle))
                    {
                        // This player is hitting this projectile
                        // TODO: BOOM!
                        // TODO: Decrement lives, etc.

                        // Deactivate just the enemy for now
                        enemy.Active = false;
                    }
                }
            }

            // Clean up any inactive projectiles
            for (int i = playerProjectiles.Count - 1; i >= 0; i-- )
            {
                if (!playerProjectiles[i].Active)
                {
                    // Remove this projectile
                    playerProjectiles.RemoveAt(i);
                }
            }

            // Clean up any inactive enemies
            for (int i = enemyEntities.Count - 1; i >= 0; i--)
            {
                if (!enemyEntities[i].Active)
                {
                    // Remove this projectile
                    enemyEntities.RemoveAt(i);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            // Draw everyone
            foreach (PlayerEntity player in playerEntities)
            {
                player.Draw(spriteBatch);
            }

            foreach (EnemyEntity enemy in enemyEntities)
            {
                enemy.Draw(spriteBatch);
            }

            foreach (ProjectileEntity projectile in playerProjectiles)
            {
                projectile.Draw(spriteBatch);
            }
        }

        #region IPlayerManager Interface

        /// <summary>
        /// Register a new player
        /// </summary>
        /// <param name="player">Player to register</param>
        public void RegisterPlayer(PlayerEntity player)
        {
            // Make sure the player isn't already registered
            if ((player != null) && (!playerEntities.Contains(player)))
            {
                playerEntities.Add(player);
            }
        }

        /// <summary>
        /// Unregister a player.  This will deactivate and remove the player from the list of players
        /// 
        /// If the player is not alrady registered, the method will fail silently.
        ///
        /// </summary>
        /// <param name="player">Player to unregister</param>
        public void UnregisterPlayer(PlayerEntity player)
        {
            if ((player != null) && (playerEntities.Contains(player)))
            {
                player.Active = false;
                playerEntities.Remove(player);
            }
        }

        /// <summary>
        /// Deactivates and removes all registered players
        /// </summary>
        public void ClearAllPlayers()
        {
            foreach (PlayerEntity player in playerEntities)
            {
                player.Active = false;
            }

            playerEntities.Clear();
        }

        #endregion

        #region IPlayerProjectileManager Interface

        /// <summary>
        /// Register a player projectile.  
        /// 
        /// Player projectiles will be updated on every update cycle.  They will also be checked for collision against
        /// all registered enemy entities
        /// </summary>
        /// <param name="playerProjectile">Projectile to register</param>
        public void RegisterPlayerProjectile(ProjectileEntity playerProjectile)
        { 
            if ((playerProjectile != null) && (!playerProjectiles.Contains(playerProjectile)))
            {
                playerProjectiles.Add(playerProjectile);
            }
        }

        /// <summary>
        /// Unregister a specific player projectile
        /// 
        /// If found, the registered player projectile will be deactivated, and removed from the list of registered projectiles
        /// </summary>
        /// <param name="playerProjectile">Projectile to unregister</param>
        public void UnregisterPlayerProjectile(ProjectileEntity playerProjectile)
        {

            if ((playerProjectile != null) && (playerProjectiles.Contains(playerProjectile)))
            {
                playerProjectile.Active = false;
                playerProjectiles.Remove(playerProjectile);
            }
        }

        /// <summary>
        /// Deactivates and removes all registered projectiles
        /// </summary>
        public void ClearAllProjectiles()
        {
            foreach (ProjectileEntity projectile in playerProjectiles)
            {
                projectile.Active = false;
            }

            playerProjectiles.Clear();
        }

        #endregion

        #region IEnemyManager Interface

        public List<PlayerEntity> PlayerEntities
        {
            get
            {
                return playerEntities;
            }
        }

        /// <summary>
        /// Register an EnemyEntity 
        /// 
        /// All enemies will be updated, and checked for collision against the PlayerEntity, and any PlayerProjectiles.
        /// </summary>
        /// <param name="enemy">Enemy to register</param>
        public void RegisterEnemy(EnemyEntity enemy)
        {
            if ((enemy != null) && (!enemyEntities.Contains(enemy)))
            {
                enemyEntities.Add(enemy);
            }
        }

        /// <summary>
        /// Unregister a specific, previously registered enemy
        /// 
        /// If found, the registered enemy will be deactivated, and removed from the list of registered enemies
        /// </summary>
        /// <param name="enemy">Enemy to unregister</param>
        public void UnregisterEnemy(EnemyEntity enemy)
        {
            if ((enemy != null) && (enemyEntities.Contains(enemy)))
            {
                enemy.Active = false;
                enemyEntities.Remove(enemy);
            }
        }

        /// <summary>
        /// Deactivates and removes all registered enemies
        /// </summary>
        public void ClearAllEnemies()
        { 
            foreach (EnemyEntity enemy in enemyEntities)
            {
                enemy.Active = false;
            }

            enemyEntities.Clear();
        }


        #endregion
    }
}
