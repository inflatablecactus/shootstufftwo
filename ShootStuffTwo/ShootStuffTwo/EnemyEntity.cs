﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ShootStuffTwo
{
    class EnemyEntity : Entity
    {
        private IEnemyManager enemyManager;
        private float mass;

        public EnemyEntity(Texture2D sprite, Vector2 location, float mass, Vector2 initialVelocity, IEnemyManager enemyManager, bool active = true, bool visible = true) : 
            base(sprite, location, initialVelocity, active, visible)
        {
            this.mass = mass;
            this.enemyManager = enemyManager;
        }

        public override void Update(GameTime gameTime)
        {
            List<PlayerEntity> playerEntities = enemyManager.PlayerEntities;

            if (playerEntities.Count > 0)
            {
                // There's at least 1 player.  Find the closest.
                PlayerEntity closestPlayer = playerEntities[0];
                for (int i = 1; i < playerEntities.Count; i++ )
                {
                    if( Vector2.Distance(playerEntities[i].Center, this.Center) < Vector2.Distance(closestPlayer.Center, this.Center))
                    {
                        // We have a new closest player
                        closestPlayer = playerEntities[i];
                    }
                }

                // Generate a thrust vector pointing from this entity to the closest player
                Vector2 enemyThrust = closestPlayer.Center - this.Center;
                enemyThrust.Normalize();
                enemyThrust *= GameConstants.ENEMY_THRUST_SCALE;

                // Apply thrust to this enemy
                // Calculate enemy drag based on current velocity
                if (velocity.Length() > 0)
                {
                    Vector2 shipDrag = (-1.0f) * Vector2.Normalize(velocity) * GameConstants.PLAYER_SHIP_IDLE_DRAG_THRUST;

                    // Combine these two forces
                    enemyThrust += shipDrag;
                }

                // Calculate the effects of the force on the mass of the ship to derive a new velocity
                // First, F = m*a, so a = F / m.
                Vector2 enemyAcceleration = enemyThrust / this.mass;

                // Now, assume we've been applying this acceleration for the entire duration of the update period
                Vector2 enemyVelocityChange = enemyAcceleration * gameTime.ElapsedGameTime.Milliseconds;

                // Resolve our new velocity as the sum of the velocity from thrust, plus the previous velocity
                velocity += enemyVelocityChange;

                // Clamp the velocity at a maximum magnitude
                if (velocity.Length() > GameConstants.ENEMY_MAX_VELOCITY)
                {
                    velocity = Vector2.Normalize(velocity) * GameConstants.ENEMY_MAX_VELOCITY;
                }

                // Update the ships position
                location += velocity * gameTime.ElapsedGameTime.Milliseconds;

                // Bounce the enemy off of the edge of the world if needed
                if (this.Left < 0)
                {
                    this.Left = 0f;
                    velocity.X *= (-1f * GameConstants.ENEMY_BOUNCE_SCALE);
                }
                else if (this.Right > GameConstants.WORLD_WIDTH)
                {
                    this.Right = GameConstants.WORLD_WIDTH;
                    velocity.X *= (-1f * GameConstants.ENEMY_BOUNCE_SCALE);
                }

                if (this.Top < 0)
                {
                    this.Top = 0f;
                    velocity.Y *= (-1f * GameConstants.ENEMY_BOUNCE_SCALE);
                }
                else if (this.Bottom >= GameConstants.WORLD_HEIGHT)
                {
                    this.Bottom = GameConstants.WORLD_HEIGHT;
                    velocity.Y *= (-1f * GameConstants.ENEMY_BOUNCE_SCALE);
                }

            }

            base.Update(gameTime);
        }
    }
}
