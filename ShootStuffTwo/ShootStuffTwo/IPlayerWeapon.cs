﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ShootStuffTwo
{
    /// <summary>
    /// The generic interface for any weapon used by a player.  
    /// </summary>
    interface IPlayerWeapon
    {
        void Shoot(IPlayerWeaponMountPoint weaponMountPoint, Vector2 firingDirection);
    }
}
