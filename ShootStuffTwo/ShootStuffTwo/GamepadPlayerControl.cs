﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ShootStuffTwo
{
    class GamepadPlayerControl : IPlayerControl
    {
        private PlayerIndex playerNumber;

        /// <summary>
        /// Default constructor
        /// </summary>
        public GamepadPlayerControl(PlayerIndex playerIndex = PlayerIndex.One)
        {
            this.playerNumber = playerIndex;
        }

        #region IPlayerControl Interface
        /// <summary>
        /// Indicates the current thrust applied to the player entity.
        /// </summary>
        public Vector2 ThrustVector
        {

            get
            {
                Vector2 thrust = Vector2.Zero;

                GamePadState currentState = GamePad.GetState(playerNumber);

                if (currentState.IsConnected)
                {
                    thrust = currentState.ThumbSticks.Left;
                    thrust.Y *= -1f;  // Controller Y axis is inverted when compared to screen coords
                    thrust *= GameConstants.PLAYER_SHIP_THRUST_SCALE;
                }

                return thrust;
            }
        }

        /// <summary>
        /// Indicates the current direction the player entity is shooting.
        /// </summary>
        public Vector2 ShootVector
        {
            get
            {
                Vector2 shoot = Vector2.Zero;

                GamePadState currentState = GamePad.GetState(playerNumber);

                if (currentState.IsConnected)
                {
                    shoot = currentState.ThumbSticks.Right;
                    shoot.Y *= -1f;  // Controller Y axis is inverted when compared to screen coords
                }

                return shoot;
            }
        }

        /// <summary>
        /// Request to detonate a smart bomb by the player.
        /// </summary>
        public bool SmartBomb
        {
            get
            {
                bool smartBomb = false;

                GamePadState currentState = GamePad.GetState(playerNumber);

                if (currentState.IsConnected)
                {
                    smartBomb = (GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed);
                }

                return smartBomb;
            }
        }
        #endregion
    }
}
