﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ShootStuffTwo
{
    class ProjectileEntity : Entity
    {
        public ProjectileEntity(Texture2D sprite, Vector2 location, Vector2 velocity, bool active = true, bool visible = true) : 
            base(sprite, location, velocity, active, visible)
        {
        }
    }
}
