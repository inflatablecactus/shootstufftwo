﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShootStuffTwo
{
    interface IPlayerProjectileManager
    {
        void RegisterPlayerProjectile(ProjectileEntity playerProjectile);

        void UnregisterPlayerProjectile(ProjectileEntity playerProjectile);

        void ClearAllProjectiles();
    }
}
