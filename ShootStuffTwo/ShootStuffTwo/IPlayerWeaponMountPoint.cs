﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ShootStuffTwo
{
    /// <summary>
    /// Interface for weapon mount point information.  A mount point is provided to any weapon as a means of obtaining 
    /// information needed for firing.
    /// </summary>
    interface IPlayerWeaponMountPoint
    {
        /// <summary>
        /// The location, in world space, of the mount point.
        /// </summary>
        Vector2 MountPointLocation { get; }

        /// <summary>
        /// The current velocity of the mount point.
        /// </summary>
        Vector2 MountPointVelocity { get; }
    }
}
