﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ShootStuffTwo
{
    /// <summary>
    /// The control-method independent interface definition for player controls.
    /// </summary>
    interface IPlayerControl
    {
        /// <summary>
        /// Indicates the current thrust applied to the player entity.
        /// </summary>
        Vector2 ThrustVector { get; }

        /// <summary>
        /// Indicates the current direction the player entity is shooting.
        /// </summary>
        Vector2 ShootVector { get; }

        /// <summary>
        /// Request to detonate a smart bomb by the player.
        /// </summary>
        bool SmartBomb { get; }
    }
}
