﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ShootStuffTwo
{
    /// <summary>
    /// The basic BallisticPlayerWeapon. This weapon fires simple ballistic projectiles.
    /// The weapon is not an entity, so it is neither Updated nor Drawn.  It is a service for creating
    /// projectile weapons after applying constraints.
    /// </summary>
    class BallisticPlayerWeapon : IPlayerWeapon
    {
        #region Fields
        private Texture2D projectileSprite;

        private DateTime timeLastFired = new DateTime();

        private IPlayerProjectileManager projectileManager;

        #endregion

        public BallisticPlayerWeapon(Texture2D sprite, IPlayerProjectileManager projectileManager)
        {
            this.projectileSprite = sprite;

            this.projectileManager = projectileManager;

            // Initialize the time we last fired
            // This basically acts like we fired a shot before the game started so we can shoot immediately
            timeLastFired = DateTime.Now;
            timeLastFired = timeLastFired.Subtract(TimeSpan.FromMilliseconds(GameConstants.BALLISTIC_PROJECTILE_WEAPON_FIRE_DELAY_MS+1));
        }

        #region IPlayerWeapon Interface

        public void Shoot(IPlayerWeaponMountPoint weaponMountPoint, Vector2 firingDirection)
        {
            // Check to see if we can shoot yet
            TimeSpan timePassedSinceLastShot = DateTime.Now - timeLastFired;

            if (timePassedSinceLastShot.TotalMilliseconds >= GameConstants.BALLISTIC_PROJECTILE_WEAPON_FIRE_DELAY_MS)
            {
                // Calculate the desired projectile velocity
                firingDirection.Normalize();
                Vector2 projectileVelocity = weaponMountPoint.MountPointVelocity + (firingDirection * GameConstants.BALLISTIC_PROJECTILE_VELOCITY);

                // Spawn a new projectile
                BallisticProjectileEntity projectile = new BallisticProjectileEntity(projectileSprite, weaponMountPoint.MountPointLocation, projectileVelocity);

                // Register it
                projectileManager.RegisterPlayerProjectile(projectile);

                // Update the time we fired
                timeLastFired = DateTime.Now;
                //Console.WriteLine("BANG! - Shot Fired! - Time: " + timeLastFired + " - Direction: " + firingDirection);
            }
            return;
        }

        #endregion
    }
}
