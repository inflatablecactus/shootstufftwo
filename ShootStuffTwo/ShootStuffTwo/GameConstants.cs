﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace ShootStuffTwo
{
    /// <summary>
    /// Container for constant values used throughout the game.
    /// </summary>
    public static class GameConstants
    {
        // resolution
        public const int WINDOW_WIDTH = 800;
        public const int WINDOW_HEIGHT = 600;

        // world characteristics
        public const float WORLD_WIDTH = WINDOW_WIDTH;
        public const float WORLD_HEIGHT = WINDOW_HEIGHT;

        // player ship characteristics
        public const float PLAYER_SHIP_MASS = 1000.0f;                                          // Mass of the player ship
        public const float PLAYER_SHIP_THRUST_SCALE = 1.0f;                                     // Scale factor for the thrust applied to the ship
        public const float PLAYER_SHIP_IDLE_DRAG_THRUST = (PLAYER_SHIP_THRUST_SCALE * 0.25f);   // Scale factor for drag applied to the ship
        public const float PLAYER_SHIP_MAX_VELOCITY = 0.5f;                                     // Maximum velocity magnitude allowed for ship movement
        public const float PLAYER_SHIP_BOUNCE_SCALE = 0.9f;                                     // Amount to scale the player velocity when bouncing off walls

        // player weapon characteristics
        public const float WEAPON_FIRING_THRESHOLD = 0.25f;                                     // Shooting stick displacement threshold for firing

        public const double BALLISTIC_PROJECTILE_WEAPON_FIRE_DELAY_MS = 100;                    // Delay in milliseconds between each shot fired
        public const float BALLISTIC_PROJECTILE_VELOCITY = 1.0f;

        public const float GAME_STATE_TRANSITION_TIME = 5000f;
        public const int ENEMY_TYPE_COUNT = 5;

        // enemy constants
        public const int ENEMY_SPAWN_PERIOD = 1000;                                              // Spawn a new enemy every Xms
        public const float ENEMY_MASS = 1000.0f;
        public const float ENEMY_THRUST_SCALE = 1.0f;
        public const float ENEMY_MAX_VELOCITY = 0.5f;
        public const float ENEMY_BOUNCE_SCALE = 0.9f;

        public const int AMOUNT_OF_FUN = 11;
    }
}
