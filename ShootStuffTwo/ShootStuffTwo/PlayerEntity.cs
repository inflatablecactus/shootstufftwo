﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ShootStuffTwo
{
    class PlayerEntity : Entity, IPlayerWeaponMountPoint
    {
        #region Fields
        /// <summary>
        /// Mass of the PlayerEntity.
        /// </summary>
        float mass;

        /// <summary>
        /// Rectangle surface used for collision detection.
        /// </summary>
        Rectangle collisionRectangle;

        /// <summary>
        /// The active IPlayerControl object.
        /// </summary>
        IPlayerControl playerControl;
        #endregion

        #region Constructors
        public PlayerEntity(Texture2D sprite, IPlayerControl playerControl, Vector2 location, float mass, Vector2 initialVelocity, bool active = true, bool visible = true) : 
            base(sprite, location, initialVelocity, active, visible)
        {
            this.mass = mass;
            this.playerControl = playerControl;

            // This is a hack to test the drawing rotation
            this.rotationalSpeed = 1f;
        }

        #endregion

        #region Properties
        /// <summary>
        /// The current weapon for this PlayerEntity.
        /// </summary>
        public IPlayerWeapon CurrentWeapon { get; set; }

        /// <summary>
        /// The rectangle used for collision detection calculations
        /// </summary>
        override public Rectangle CollisionRectangle
        {
            get { return collisionRectangle; }
        }

        #endregion

        #region IPlayerWeaponMountPoint Interface

        /// <summary>
        /// The MountPointLocation on this PlayerEntity.  
        /// </summary>
        public Vector2 MountPointLocation 
        { 
            get
            {
                return Center;
            }
        }

        /// <summary>
        /// The MountPointVelocity on this PlayerEntity.  
        /// </summary>
        public Vector2 MountPointVelocity 
        { 
            get
            {
                return velocity;
            } 
        }

        #endregion

        #region Methods

        /// <summary>
        /// Update all PlayerEntity state information.
        /// </summary>
        /// <param name="gameTime">GameTime interval representing time passed since the last update.</param>
        override public void Update(GameTime gameTime)
        {
            UpdateRotation(gameTime);

            UpdateMovement(gameTime);

            UpdateWeapons(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// Draw the PlayerEntity, and any sub-entities.
        /// </summary>
        /// <param name="spriteBatch">Active SpriteBatch for drawing operations.</param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (this.Visible)
            {
                spriteBatch.Draw(sprite, location, null, Color.White, rotationalAngle, spriteOrigin, 1f, SpriteEffects.None, 1f);
            }
        }


        /// <summary>
        /// Updates the rotation of the entity
        /// </summary>
        /// <param name="gameTime">GameTime interval representing time passed since the last update.</param>
        private void UpdateRotation(GameTime gameTime)
        {
            // Calculate the angle, in radians, between a unit vector pointing up, and the current thrust vector 
            Vector2 shipThrust = playerControl.ThrustVector;

            // Only adjust the angle if thrust is being applied
            if (shipThrust.Length() > 0)
            {
                // Start with our reference vector, which is pointing up
                Vector2 reference = Vector2.UnitY * (-1f);

                // Normalize our thrust vector
                shipThrust.Normalize();

                float dotProduct = Vector2.Dot(shipThrust, reference);

                float angle = (float)Math.Acos(dotProduct);

                // Adjust for situations where we're pushing to the left
                if (playerControl.ThrustVector.X > 0)
                {
                    rotationalAngle = angle;
                }
                else
                {
                    rotationalAngle = MathHelper.TwoPi - angle;
                }
            }
        }

        /// <summary>
        /// Updates all aspects of the PlayerEntity movement.
        /// </summary>
        /// <param name="gameTime">GameTime interval representing time passed since the last update.</param>
        private void UpdateMovement(GameTime gameTime)
        {
            // Calculate ship thrust from player control
            Vector2 shipThrust = playerControl.ThrustVector;

            // Calculate ship drag based on current velocity
            if (velocity.Length() > 0)
            {
                Vector2 shipDrag = (-1.0f) * Vector2.Normalize(velocity) * GameConstants.PLAYER_SHIP_IDLE_DRAG_THRUST;

                // Combine these two forces
                shipThrust += shipDrag;
            }

            // Calculate the effects of the force on the mass of the ship to derive a new velocity
            // First, F = m*a, so a = F / m.
            Vector2 shipAcceleration = shipThrust / mass;

            // Now, assume we've been applying this acceleration for the entire duration of the update period
            Vector2 shipVelocityChange = shipAcceleration * gameTime.ElapsedGameTime.Milliseconds;

            // Resolve our new velocity as the sum of the velocity from thrust, plus the previous velocity
            velocity += shipVelocityChange;

            // Clamp the velocity at a maximum magnitude
            if (velocity.Length() > GameConstants.PLAYER_SHIP_MAX_VELOCITY)
            {
                velocity = Vector2.Normalize(velocity) * GameConstants.PLAYER_SHIP_MAX_VELOCITY;
            }

            // Update the ships position
            location += velocity * gameTime.ElapsedGameTime.Milliseconds;

            // Bounce the ship off of the edge of the world if needed
            if (this.Left < 0)
            {
                this.Left = 0f;
                velocity.X *= (-1f * GameConstants.PLAYER_SHIP_BOUNCE_SCALE);
            }
            else if (this.Right > GameConstants.WORLD_WIDTH)
            {
                this.Right = GameConstants.WORLD_WIDTH;
                velocity.X *= (-1f * GameConstants.PLAYER_SHIP_BOUNCE_SCALE);
            }

            if (this.Top < 0)
            {
                this.Top = 0f;
                velocity.Y *= (-1f * GameConstants.PLAYER_SHIP_BOUNCE_SCALE);
            }
            else if (this.Bottom >= GameConstants.WORLD_HEIGHT)
            {
                this.Bottom = GameConstants.WORLD_HEIGHT;
                velocity.Y *= (-1f * GameConstants.PLAYER_SHIP_BOUNCE_SCALE);
            }

            // Update the collision rectangle.
            // TODO: This is imprecise.  I need better collision detection.
            collisionRectangle.X = (int)location.X;
            collisionRectangle.Y = (int)location.Y;
            collisionRectangle.Height = sprite.Height;
            collisionRectangle.Width = sprite.Width;
        }

        /// <summary>
        /// Updates all aspects of weapon interaction for the PlayerEntity.
        /// </summary>
        /// <param name="gameTime">GameTime value from framework</param>
        private void UpdateWeapons(GameTime gameTime)
        {
            if (CurrentWeapon != null)
            {
                Vector2 currentShootVector = playerControl.ShootVector;

                if (currentShootVector.Length() >= GameConstants.WEAPON_FIRING_THRESHOLD)
                {
                    CurrentWeapon.Shoot(this, currentShootVector);
                }
            }
        }
        #endregion
    }
}
