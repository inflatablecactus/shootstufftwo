﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ShootStuffTwo
{
    /// <summary>
    /// The base class for all objects handled by the game.
    /// </summary>
    class Entity
    {
        #region Fields

        /// <summary>
        /// Linear velocity of the entity
        /// </summary>
        protected Vector2 velocity;

        /// <summary>
        /// Rotational speed of the entity in radians per second
        /// </summary>
        protected float rotationalSpeed;

        /// <summary>
        /// Angle of rotation from the default sprite orientation;
        /// </summary>
        protected float rotationalAngle;

        /// <summary>
        /// The location, in world coordinates, of the center Entity.
        /// </summary>
        protected Vector2 location;

        /// <summary>
        /// 2d texture used to render the sprite for the entity
        /// </summary>
        protected Texture2D sprite;

        /// <summary>
        /// The sprite origin for the entity.  This entity system uses center references, so the spriteOrigin will be the offset from the 
        /// top left corner to the center of the sprite.
        /// </summary>
        protected Vector2 spriteOrigin;

        /// <summary>
        /// The drawRectangle, in screen coordinates, for the entity
        /// </summary>
        protected Rectangle drawRectangle;


        #endregion

        #region Constructors

        /// <summary>
        /// Constructor for Entity
        /// </summary>
        /// <param name="sprite">Sprite to render when drawn</param>
        /// <param name="location">Location of the Entity, in world coordinates</param>
        /// <param name="active">Flag to set the active state of the Entity at construction</param>
        /// <param name="visible">Flag to set wither or not the Entity is visible at construction</param>
        protected Entity(Texture2D sprite, Vector2 location, Vector2 velocity, bool active = true, bool visible = true)
        {
            this.sprite = sprite;

            this.spriteOrigin.X = sprite.Width / 2;
            this.spriteOrigin.Y = sprite.Height / 2;

            this.location = location;

            this.velocity = velocity;
            this.rotationalSpeed = 0f;
            this.rotationalAngle = 0f;

            drawRectangle = new Rectangle((int)location.X - sprite.Width / 2,
                                          (int)location.Y - sprite.Height / 2, 
                                          sprite.Width,
                                          sprite.Height);

            Active = active;
            Visible = visible;
        }

        #endregion

        #region Properties
        /// <summary>
        /// The rectangle used for collision detection calculations
        /// </summary>
        virtual public Rectangle CollisionRectangle
        {
            get { return drawRectangle; }
        }

        /// <summary>
        /// Determines whether or not this entity is updated during Update
        /// </summary>
        virtual public bool Active { get; set; }

        /// <summary>
        /// Determines whether or not this entity is drawn during Draw
        /// </summary>
        virtual public bool Visible { get; set; }

        /// <summary>
        /// World coordinates of the left side of the entity 
        /// </summary>
        virtual public float Left
        {
            get
            {
                return location.X - sprite.Width / 2;
            }

            set
            {
                location.X = value + sprite.Width / 2;
            }
        }

        /// <summary>
        /// World coordinates of the right side of the entity
        /// </summary>
        virtual public float Right
        {
            get
            {
                return location.X + sprite.Width / 2;
            }

            set
            {
                location.X = value - sprite.Width / 2;
            }
        }

        /// <summary>
        /// World coordinates of the top edge of the entity
        /// </summary>
        virtual public float Top
        {
            get
            {
                return location.Y - sprite.Height / 2;
            }

            set
            {
                location.Y = value + sprite.Height / 2;
            }
        }

        /// <summary>
        /// World coordinates of the bottom edge of the entity
        /// </summary>
        virtual public float Bottom
        {
            get
            {
                return location.Y + sprite.Height / 2;
            }

            set
            {
                location.Y = value - sprite.Height / 2;
            }
        }

        /// <summary>
        /// World coordinates of the center of the entity
        /// </summary>
        virtual public Vector2 Center
        {
            get 
            {
                return location;
            }

            set
            {
                location = value;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Update method to be called on the Entity during the Game Update cycle.
        /// </summary>
        /// <param name="gametime">gameTime value from Game update</param>
        virtual public void Update(GameTime gametime)
        {

        }

        /// <summary>
        /// Method to draw the entity
        /// </summary>
        /// <param name="spriteBatch">Current active spriteBatch.  The spriteBatch must already be started outside of the Entity.</param>
        virtual public void Draw(SpriteBatch spriteBatch)
        {
            // Update the drawRectangle based on current worldLocation
            drawRectangle.X = (int)location.X - sprite.Width / 2;
            drawRectangle.Y = (int)location.Y - sprite.Height / 2;

            if (this.Visible)
            {
                spriteBatch.Draw(sprite, drawRectangle, Color.White);
            }
        }

        #endregion
    }
}
