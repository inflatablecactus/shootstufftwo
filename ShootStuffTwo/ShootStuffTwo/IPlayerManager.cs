﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShootStuffTwo
{
    interface IPlayerManager
    {
        void RegisterPlayer(PlayerEntity player);

        void UnregisterPlayer(PlayerEntity player);

        void ClearAllPlayers();
    }
}
