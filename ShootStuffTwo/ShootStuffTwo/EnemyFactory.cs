﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ShootStuffTwo
{
    class EnemyFactory
    {
        #region FIELDS
        private int nextEnemySpawnCounter = 0;
        private List<Texture2D> enemySprites;
        private IEnemyManager enemyManager;
        #endregion

        #region CONSTRUCTORS
        public EnemyFactory(List<Texture2D> enemySprites, IEnemyManager enemyManager)
        {
            this.enemySprites = enemySprites;
            this.enemyManager = enemyManager;
        }
        #endregion

        #region METHODS
        /// <summary>
        /// Update method by the main engine.  All logic for determining when to spawn new enemies is in this method.
        /// </summary>
        /// <param name="gameTime">GameTime object from main Game update loop.</param>
        public void Update(GameTime gameTime)
        {
            nextEnemySpawnCounter += gameTime.ElapsedGameTime.Milliseconds;
            if (nextEnemySpawnCounter >= GameConstants.ENEMY_SPAWN_PERIOD)
            {
                // Reset our counter
                nextEnemySpawnCounter = 0;

                // Spawn a new enemy
                EnemyEntity newEnemy = new EnemyEntity(enemySprites[0], 
                    new Vector2(GameConstants.WORLD_WIDTH / 2, GameConstants.WORLD_HEIGHT / 2), 
                    GameConstants.ENEMY_MASS, 
                    new Vector2(0, 0),
                    enemyManager);

                enemyManager.RegisterEnemy(newEnemy);
            }
        }
        #endregion
    }
}
