﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using TinyIoC;

namespace ShootStuffTwo
{
    interface IGameStateHandler
    {
        void Update(GameTime gameTime);

        void Draw(SpriteBatch spriteBatch);

        void EnterState();

        void ExitState();
    }

    class GameStateMachine
    {
        private Game1 game;

        public enum GameStates
        {
            Attract,
            Menu,
            NewGame,
            Game,
            GameOver
        }

        Dictionary<GameStates, IGameStateHandler> gameStateHandlers;

        public GameStateMachine(Game1 game)
        {
            this.game = game;

            // Initialize our dictionary of state handlers
            gameStateHandlers = new Dictionary<GameStates, IGameStateHandler> 
            {
                {GameStates.Attract, new AttractHandler(this)},
                {GameStates.Menu, new MenuHandler(this)},
                {GameStates.NewGame, new NewGameHandler(this)},
                {GameStates.Game, new GameHandler(this)},
                {GameStates.GameOver, new GameOverHandler(this)}
            };

            // Set our initial state to Attract
            _currentState = GameStates.Attract;

            // We need to manually invoke our EnterState at construction, but we can 
            // rely on our CurrentState property to handle calling EnterState and ExitState in the future
            gameStateHandlers[_currentState].EnterState();
        }

        private GameStates _currentState;
        public GameStates CurrentState 
        { 
            get
            {
                return _currentState;
            }
            
            set
            {
                if (value != this._currentState)
                {
                    // Call the ExitState method for the current state
                    gameStateHandlers[this._currentState].ExitState();

                    // Assign the new state value
                    this._currentState = value;

                    // Call the EnterState method for our new state
                    gameStateHandlers[this._currentState].EnterState();
                }
            }
        }

        public void Update(GameTime gameTime)
        {
            gameStateHandlers[CurrentState].Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            gameStateHandlers[CurrentState].Draw(spriteBatch);
        }

        #region ATTRACT

        class AttractHandler : IGameStateHandler 
        {
            private GameStateMachine stateMachine;

            public AttractHandler(GameStateMachine stateMachine) 
            {
                this.stateMachine = stateMachine;
            }

            public void Update(GameTime gameTime)
            {
            }

            public void Draw(SpriteBatch spriteBatch)
            {
            }

            public void EnterState()
            {
                Console.WriteLine("Enter State " + stateMachine.CurrentState);
            }

            public void ExitState()
            {
                Console.WriteLine("Exit State " + stateMachine.CurrentState);
            }
        }

        #endregion

        #region MENU

        class MenuHandler : IGameStateHandler
        {
            private GameStateMachine stateMachine;

            public MenuHandler(GameStateMachine stateMachine) 
            {
                this.stateMachine = stateMachine;
            }

            public void Update(GameTime gameTime)
            {
            }

            public void Draw(SpriteBatch spriteBatch)
            {
            }

            public void EnterState()
            {
                Console.WriteLine("Enter State " + stateMachine.CurrentState);
            }

            public void ExitState()
            {
                Console.WriteLine("Exit State " + stateMachine.CurrentState);
            }
        }

        #endregion

        #region NEW GAME

        class NewGameHandler : IGameStateHandler
        {
            private GameStateMachine stateMachine;

            public NewGameHandler(GameStateMachine stateMachine) 
            {
                this.stateMachine = stateMachine;
            }

            public void Update(GameTime gameTime)
            {
            }

            public void Draw(SpriteBatch spriteBatch)
            {

            }

            public void EnterState()
            {
                Console.WriteLine("Enter State " + stateMachine.CurrentState);
            }

            public void ExitState()
            {
                Console.WriteLine("Exit State " + stateMachine.CurrentState);
            }
        }

        #endregion

        #region GAME

        class GameHandler : IGameStateHandler
        {
            private GameStateMachine stateMachine;

            private EntityManager entityManager;

            private PlayerEntity playerEntity;

            private EnemyFactory enemyFactory;

            public GameHandler(GameStateMachine stateMachine) 
            {
                this.stateMachine = stateMachine;
            }

            public void Update(GameTime gameTime)
            {
                if (entityManager != null)
                {
                    entityManager.Update(gameTime);
                }

                if (enemyFactory != null)
                {
                    enemyFactory.Update(gameTime);
                }
            }

            public void Draw(SpriteBatch spriteBatch)
            {
                if (entityManager != null)
                {
                    entityManager.Draw(spriteBatch);
                }
            }

            public void EnterState()
            {
                Console.WriteLine("Enter State " + stateMachine.CurrentState);

                var playerControl = TinyIoCContainer.Current.Resolve<IPlayerControl>();

                // Create an EntityManager
                entityManager = new EntityManager();

                // Create an EnemyFactory
                enemyFactory = new EnemyFactory(stateMachine.game.enemySprites, entityManager);

                // Create a PlayerEntity
                playerEntity = new PlayerEntity(stateMachine.game.playerSprite,
                    playerControl,
                    new Vector2(GameConstants.WINDOW_WIDTH/2, GameConstants.WINDOW_HEIGHT/2),
                    GameConstants.PLAYER_SHIP_MASS,
                    new Vector2(0f),
                    true,
                    true);

                // Create a weapon
                var ballisticWeapon = new BallisticPlayerWeapon(stateMachine.game.projectileSprite, entityManager);

                // Attach the weapon to the player
                playerEntity.CurrentWeapon = ballisticWeapon;

                // Register the player
                entityManager.RegisterPlayer(playerEntity);
            }

            public void ExitState()
            {
                Console.WriteLine("Exit State " + stateMachine.CurrentState);
            }
        }

        #endregion

        #region GAME OVER

        class GameOverHandler : IGameStateHandler
        {
            private float elapsedTime;
            private GameStateMachine stateMachine;

            public GameOverHandler(GameStateMachine stateMachine) 
            {
                this.stateMachine = stateMachine;
            }

            public void Update(GameTime gameTime)
            {
                elapsedTime += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

                Console.WriteLine("Current State = " + stateMachine.CurrentState);

                if (elapsedTime > GameConstants.GAME_STATE_TRANSITION_TIME)
                {
                    // Change states
                    stateMachine.CurrentState = GameStates.Attract;
                }
            }

            public void Draw(SpriteBatch spriteBatch)
            {

            }

            public void EnterState()
            {
                Console.WriteLine("Enter State " + stateMachine.CurrentState);
                elapsedTime = 0.0f;
            }

            public void ExitState()
            {
                Console.WriteLine("Exit State " + stateMachine.CurrentState);
            }
        }

        #endregion
    }
}
