﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShootStuffTwo
{
    interface IEnemyManager
    {
        void RegisterEnemy(EnemyEntity enemy);

        void UnregisterEnemy(EnemyEntity enemy);

        List<PlayerEntity> PlayerEntities
        {
            get;
        }

        void ClearAllEnemies();
    }
}
