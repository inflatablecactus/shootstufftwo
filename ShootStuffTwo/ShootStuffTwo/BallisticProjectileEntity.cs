﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ShootStuffTwo
{
    /// <summary>
    /// A single ballistic projectile.  Key ingredient required when shooting stuff.
    /// </summary>
    class BallisticProjectileEntity : ProjectileEntity
    {
        #region Constructors

        public BallisticProjectileEntity(Texture2D sprite, Vector2 location, Vector2 velocity, bool active = true, bool visible = true) : 
            base(sprite, location, velocity, active, visible)
        {
        }

        #endregion

        public override void Update(GameTime gameTime)
        {
            // Update the projectile position
            location += velocity * gameTime.ElapsedGameTime.Milliseconds;

            base.Update(gameTime);
        }
    }
}
